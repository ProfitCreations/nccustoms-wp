<?php
/**
 * NcCustoms Theme Customizer.
 *
 * @package NcCustoms
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function nc__customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	// Add our social link options.
    $wp_customize->add_section(
        'nc__social_links_section',
        array(
            'title'       => esc_html__( 'Social Links', 'nc-customs' ),
            'description' => esc_html__( 'These are the settings for social links. Please limit the number of social links to 5.', 'nc-customs' ),
            'priority'    => 90,
        )
    );

    // Create an array of our social links for ease of setup.
    $social_networks = array( 'facebook', 'googleplus', 'instagram', 'linkedin', 'twitter' );

    // Loop through our networks to setup our fields.
    foreach( $social_networks as $network ) {

	    $wp_customize->add_setting(
	        'nc__' . $network . '_link',
	        array(
	            'default' => '',
	            'sanitize_callback' => 'nc__sanitize_customizer_url'
	        )
	    );
	    $wp_customize->add_control(
	        'nc__' . $network . '_link',
	        array(
	            'label'   => sprintf( esc_html__( '%s Link', 'nc-customs' ), ucwords( $network ) ),
	            'section' => 'nc__social_links_section',
	            'type'    => 'text',
	        )
	    );
    }

    // Add our Footer Customization section section.
    $wp_customize->add_section(
        'nc__footer_section',
        array(
            'title'    => esc_html__( 'Footer Customization', 'nc-customs' ),
            'priority' => 90,
        )
    );

    // Add our copyright text field.
    $wp_customize->add_setting(
        'nc__copyright_text',
        array(
            'default' => ''
        )
    );
    $wp_customize->add_control(
        'nc__copyright_text',
        array(
            'label'       => esc_html__( 'Copyright Text', 'nc-customs' ),
            'description' => esc_html__( 'The copyright text will be displayed beneath the menu in the footer.', 'nc-customs' ),
            'section'     => 'nc__footer_section',
            'type'        => 'text',
            'sanitize'    => 'html'
        )
    );
}
add_action( 'customize_register', 'nc__customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function nc__customize_preview_js() {
    wp_enqueue_script( 'nc__customizer', get_template_directory_uri() . '/assets/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'nc__customize_preview_js' );

/**
 * Sanitize our customizer text inputs.
 */
function nc__sanitize_customizer_text( $input ) {
    return sanitize_text_field( force_balance_tags( $input ) );
}

/**
 * Sanitize our customizer URL inputs.
 */
function nc__sanitize_customizer_url( $input ) {
    return esc_url( $input );
}
